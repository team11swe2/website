function heartbeatRequest()
    var id, text;

    id = document.getElementById("gatewayId").value;

    if (isNaN(id) || id < 1 || id > 9999)
    {
        text = "Not a valid Gateway Id";
    }
    else
    {
        var xmlhttp = new XMLHttpRequest();

        xmlhttp.onreadystatechange = function()
        {
            if (this.readyState == 4 && this.status == 200)
            {
                var myObj = JSON.parse(this.responseText);
                document.getElementById(“heartbeat”).innerHTML = JSON.stringify(myObj);
            }
        };
        xmlhttp.open("GET", "https://team11.dev.softwareengineeringii.com/api?gid=" + id, true);
        xmlhttp.send();
    }
    document.getElementById("heartbeat").innerHTML = text;


//function registerGateway()
//{
//    var id, text;
//
//    session_id = document.getElementById("registerGW").value;
//
//    if (isNaN(id) || id < 1 || id > 9999)
//    {
//        text = "Not a valid Gateway Id";
//    }
//    else
//    {
//        var xmlhttp = new XMLHttpRequest();
//
//        xmlhttp.onreadystatechange = function()
//        {
//            if (this.readyState == 4 && this.status == 200)
//            {
//                var myObj = JSON.parse(this.responseText);
//                document.getElementById(“register”).innerHTML = JSON.stringify(myObj);
//            }
//        };
//        xmlhttp.open("GET", "https://team11.dev.softwareengineeringii.com/api/auth/newgateway, true);
//        xmlhttp.send();
//    }
//    document.getElementById("register").innerHTML = text;
//}

//function login() {
//  var user = document.getElementById("user").value;
//  var pass = document.getElementById("pass").value;
//
//  const xhr = new XMLHttpRequest();
//
//  xhr.onload = function(){
//    const serverResponse = document.getElemntById('serverResponse');
//    serverResponse.innerHTML = this.responseText;
//  });
//
//  xhr.open("POST", " https://team11.dev.softwareengineeringii.com/api/auth/login");
//  xhr.setRequestHeader("username" , user)
//  xhr.setRequestHeader("password" , pass)
//  xhr.withCredentials = true;
//  xhr.send();
//
//  // NOT SURE ABOUT THIS..
//  xhr.onreadystatechange = function() {
//      //Call a function when the state changes.
//  if(xhr.readyState == 4 && req.status == 200){
//  		alert(xhr.responseText);
//  }
//}

//OLD JAVA..
//function signUp() {
//
//  var user = document.getElementById("user").value;
//  var pass = document.getElementById("pass").value;
//
//  const xhr = new XMLHttpRequest();
//
//  xhr.onload = function(){
//    const serverResponse = document.getElemntById('serverResponse');
//    serverResponse.innerHTML = this.responseText;
//  });
//
//  xhr.open("POST", " https://team11.dev.softwareengineeringii.com/api/auth/signup");
//  xhr.setRequestHeader("username" , user)
//  xhr.setRequestHeader("password" , pass)
//  xhr.withCredentials = true;
//  xhr.send();
//
//  xhr.onreadystatechange = function() {
//       if(xhr.readyState == 4 && http.status == 200) {
//        		alert(xhr.responseText);
//       }
//  }
//}

//=================================================================
//Function to validate Sign Up and Log In input by the user
// Once input is okay then it will post to the CIC
// If validate == good then it will call function signUp()??
//function validation(){
//
//    var first = document.getElementById("first").value;
//    var last = document.getElementById("last").value;
//    var user = document.getElementById("user").value;
//    var pass = document.getElementById("pass").value;
//    var confirmPass = document.getElementById("conpass").value;
//    var emial = document.getElementById("email").value;
//
//
//    if (first == ""){
//        document.getElementById('firstName').innerHTML =
//            "Please fill in First Name";
//        return false;
//    }
//    if (last == ""){
//        document.getElementById('lastName').innerHTML =
//            "Please fill in Last Name";
//        return false;
//    }
//    if (user == ""){
//        document.getElementById('username').innerHTML =
//            "Please fill in Username";
//        return false;
//    }
//    if (user.length <= 2 || (user.length > 20)){
//        document.getElementById('username').innerHTML =
//            "Username must be between 2 and 20 characters";
//        return false;
//    }
//    if (!isNaN(user)){
//        document.getElementById('username').innerHTML =
//            "Only characters are allowed";
//        return false;
//    }
//    if (pass == ""){
//        document.getElementById('password').innerHTML =
//            "Please fill in Password";
//        return false;
//    }
//    if ((pass.length) <= 5 || (pass.lenth > 20)){
//        document.getElementById('password').innerHTML =
//            "Password must be length 5 and 20";
//        return false;
//    }
//    if (pass != confirmPass){
//        document.getElementById('password').innerHTML =
//            "Passwords need to match";
//        return false;
//    }
//    if (confirmPass == ""){
//        document.getElementById('confirmPassword').innerHTML =
//            "Please fill in Confirm Password";
//        return false;
//    }
//    if (email == ""){
//        document.getElementById('emailId').innerHTML =
//            "Please fill in Email";
//        return false;
//    }
//    if (email.indexOf('@') <= 0){
//        document.getElementById('emailId').innerHTML =
//            "Invalid email";
//        return false;
//    }
//
//    //e.g. vbald@gmail.com
//    if ((email.charAt(email.length-4)!= '.') &&(email.charAt(email.length-3)!= '.')){
//        document.getElementById('emailId').innerHTML =
//            "Invalid email";
//        return false;
//    }
//
//    // Once validation == true, it should call function signUp() to post to CIC.
//    if (validation() == true)
//    {
//        return signUp();
//    }
//    }
//}