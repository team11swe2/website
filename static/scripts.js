

//=================================================================
//              manageGW.html page
//=================================================================
// Function for Request New Gateway
// GET method
// Endpoint: /api/auth/newgateway
// HEADERS: sid (session id)
// Response: JSON { token : gw_token }
// GOOD --> WORKS

function registerGateway() {
	var sessionIdCookie = document.cookie.substring(10);
	var host = window.location.hostname;
    const registerArea = $('#registerArea');

    //AJAX GET request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
    $.ajax({
    url: "https://" + host + "/api/auth/newgateway",
    type: "GET",
    // Setting Headers
    beforeSend: function(request) {
        request.setRequestHeader("sid" , sessionIdCookie);
        },
    data: "",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (response) {
        /** @namespace response.data */
        //N.B. JavaScript will **automatically** parse JSON strings into Objects!

        // Need to check if this works with storing a Token into a cookie
		 var gw_token = "gw_token" + response.token; //storing token to a cookie?
         document.token = gw_token;

        document.getElementById("registerArea").innerHTML = response.token;

    },
    error: function (data, status, error) {
        console.log(data, status, error);
        document.getElementById("registerArea").innerHTML = ('Error Requesting New Gateway! Please try again Later.');
        //alert("Error Requesting New Gateway!");
    }
    });
}

//=================================================================
//Function Get Gateways associated with SessionId and place it on Table
// GET Method
// Endpoint: /api/auth/gateways
// Response: JSON [ token_1, token_2 ... token_n ]
// HEADERS:  sid (session id)
// GOOD --> WORKS

function getGateways() {
	var host = window.location.hostname;
	var sessionIdCookie = document.cookie.substring(10);

	const gatewayArea = $('#gatewayArea');

    //AJAX GET request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
    $.ajax({
    url: "https://" + host + "/api/auth/gateways",
    type: "GET",
    // Setting Headers
    beforeSend: function(request) {
        request.setRequestHeader("sid" , sessionIdCookie);
        },
    data: "",
    contentType: "application/json; charset=utf-8",
    dataType: "json",
    success: function (response) {
        /** @namespace response.data */
        //N.B. JavaScript will **automatically** parse JSON strings into Objects!
        let jsonString = JSON.stringify(response); // changed stringify to parse
        document.getElementById("gatewayArea").innerHTML = String(response).split(',').join('\n');
    },
    error: function (data, status, error) {
        console.log(data, status, error);
        document.getElementById("gatewayArea").innerHTML = ('Error Displaying Gateway Id! Please try again Later.');
        //alert("Error Displaying Gateways!");
    }
    });
}

//=================================================================
// Function to Get Heartbeats
// GET method
// Endpoint: /api/CiC/heartbeat
// Returns: GW Heartbeat JSON
// Headers: gwToken, sessionId
// DOUBLE CHECK

function getHeartbeats(){

	var host = window.location.hostname;
	var sessionIdCookie = document.cookie.substring(10);
    var gwToken = $('#gw').val();
	const heartbeatArea = $('#heartbeatArea');

    //AJAX GET request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
   $.ajax({
       url: "https://" + host + "/api/CiC/heartbeat",
       type: "GET",
       //data: "",
       contentType: "application/json; charset=utf-8",
       beforeSend: function(request) {
          request.setRequestHeader("gwToken", gwToken)
          request.setRequestHeader("sessionId", sessionIdCookie)
                       },
       dataType: "json",
       success: function (response) {
       /** @namespace response.data */
       //N.B. JavaScript will **automatically** parse JSON strings into Objects!

       // New Code Added
       showHeartbeats(response);

       },
       error: function (data, status, error) {
           console.log(data, status, error);
           document.getElementById("heartbeatArea").innerHTML = ('Error Loading Heartbeats! Please try again Later.');
           //alert ("Error Loading Heartbeats!");
       }
   });
}

// This is a helper method meant for creating
// a table of heartbeats within the popup
function showHeartbeats(heartbeats){
    var hbArea = document.getElementById("heartbeatArea");
	hbArea.innerHTML = "";
    tbl  = document.createElement('table');
    tbl.style.width  = 'inherit';

    var tr = tbl.insertRow();
    var rc = tr.insertCell();
	rc.appendChild(document.createTextNode('Timestamp'));

    for(i in heartbeats){
	    console.log(heartbeats[i]);
    	tr = tbl.insertRow();
    	rc = tr.insertCell();
		var date = new Date(heartbeats[i].timestamp);
    	rc.appendChild(document.createTextNode(date));

    }
	tbl.classList.add('table')
    hbArea.appendChild(tbl);
}

//=================================================================
// Function to Get Diagnostics
// GET method
// Endpoint: /api/CiC/diagnostics
// Returns: Diagnostic Info JSON
// Headers: gwToken, sessionId
// NEED WORK

function getDiagnostics(){ // Changed diagnosticRequest to getDiagnostics
	var host = window.location.hostname;
	var sessionIdCookie = document.cookie.substring(10);
    var gwToken = $('#gatewayId').val();
    const diagnosticsArea = $('#diagnosticsArea');

    //AJAX GET request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
   $.ajax({
       url: "https://" + host + "/api/CiC/diagnostics",
       type: "GET",
       data: "",
       contentType: "application/json; charset=utf-8",
       beforeSend: function(request) {
          request.setRequestHeader("gwToken", gwToken)
          request.setRequestHeader("sessionId", sessionIdCookie)
                       },
       dataType: "json",
       success: function (response) {
       /** @namespace response.data */
       //N.B. JavaScript will **automatically** parse JSON strings into Objects!
	   showDiagnostics(response);
       },
       error: function (data, status, error) {
           console.log(data, status, error);
           document.getElementById("diagnosticsArea").innerHTML = ('Error Loading Diagnostics! Please try again Later.');
           //alert ("Error Loading Diagnostics!");
       }
   });
}

function showDiagnostics(heartbeats){
    var hbArea = document.getElementById("diagnosticsArea");
	hbArea.innerHTML = "";
    tbl  = document.createElement('table');
    tbl.style.width  = 'inherit';

    var tr = tbl.insertRow();
    var rc = tr.insertCell();
	rc.appendChild(document.createTextNode('Diagnostics Result'));

    for(i in heartbeats){
	    console.log(heartbeats[i]);
    	tr = tbl.insertRow();
    	rc = tr.insertCell();
    	rc.appendChild(document.createTextNode(heartbeats[i].diagnostics));

    }
	tbl.classList.add('table')
    hbArea.appendChild(tbl);
}

//=================================================================
// Function for On Demand Diagnostics
// POST Method
// Endpoint: /api/CiC/diagnosticReq
// Returns: 'status': 'REQUEST RECEIVED'
// Headers: gwToken, sessionId , diagName
// GOOD --> WORKS

// We'll want to make sure any forms on the page aren't allowed to submit and reload the page
document.addEventListener("DOMContentLoaded", function (event) {
    $("onDemandDiagnosticArea").submit(function (e) {
        e.preventDefault(e);//this will prevent forms from submitting and the page from reloading
    });
});
function diagnosticRequest() {
	var host = window.location.hostname;
	var sessionIdCookie = document.cookie.substring(10);
	var gwToken = $('#gatewayId').val();
	var diagName = $('#name').val();
	const diagnosticsArea = $('#onDemandDiagnosticArea');

    //AJAX POST request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
        $.ajax({
            url: "https://" + host + "/api/CiC/diagnosticReq",
            type: "POST",
            // Setting Headers
            beforeSend: function(request) {
                request.setRequestHeader("gwToken", gwToken)
                request.setRequestHeader("sessionId", sessionIdCookie)
                request.setRequestHeader("diagName", diagName)
                    },

            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (response) {
                //N.B. JavaScript will **automatically** parse JSON strings into Objects!
                //This block checks to see if the AJAX request was successful, but we still need to
                //make sure our response payload doesn't have any errors
                let jsonString = JSON.stringify(response);
                document.getElementById("onDemandDiagnosticArea").innerHTML = response.status;
            },
            error: function (data, status, error) {
                console.log(data, status, error);
                document.getElementById("onDemandDiagnosticArea").innerHTML = ('Error Requesting Diagnostic! Please try again Later.');
                //alert("Error Requesting Diagnostic! Please Try Again.");
            }
        });
    }

//=================================================================
// Function New Diagnostic Test
// POST Method
// Endpoint: /api/CiC/diagnosticTest
// Returns: 'status': 'INSERTED'
// Headers: Key: gwToken, Key: sessionId, Key: diagName
// NEED WORK

// We'll want to make sure any forms on the page aren't allowed to submit and reload the page
document.addEventListener("DOMContentLoaded", function (event) {
    $("testDiagnostic").submit(function (e) {
        e.preventDefault(e);//this will prevent forms from submitting and the page from reloading
    });
});
function diagnosticTest() {
	var host = window.location.hostname;
	var sessionIdCookie = document.cookie.substring(10);
    var gwToken = $('#gid').val();
	var diagName = $('#dName').val();

	const diagnosticTestArea = $('diagnosticTestArea');

    //AJAX POST request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
        $.ajax({
            url: "https://" + host + "/api/CiC/diagnosticTest",
            type: "POST",
            // Setting Headers
            beforeSend: function(request) {
                request.setRequestHeader("gwToken", gwToken)
                request.setRequestHeader("sessionId", sessionIdCookie)
                request.setRequestHeader("diagName", diagName)
                    },

            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (response) {
                /** @namespace response.data
                 *  @namespace response.status
                 */
                //N.B. JavaScript will **automatically** parse JSON strings into Objects!
                //This block checks to see if the AJAX request was successful, but we still need to
                //make sure our response payload doesn't have any errors
                let jsonString = JSON.stringify(response);
				document.getElementById("diagnosticTestArea").innerHTML = response;


            },
            error: function (data, status, error) {
                console.log(data, status, error);
                document.getElementById("diagnosticTestArea").innerHTML = ('Error Posting Diagnostic Test! Please try again Later.');
                //alert("Error Posting Diagnostic Test! Please Try Again.");
            }
        });
    }

//=================================================================
// Function to Schedule Diagnostic
// POST Method
// Endpoint: /api/CiC/diagnosticReq
// HEADERS: gwToken , sessionId , diagName , scheduled
// Returns: 'REQUEST RECEIVED'
// Status of function

function scheduledDiagnostic(){

	var host = window.location.hostname;
	var sessionIdCookie = document.cookie.substring(10);
    var gwToken = $('#gwId').val();
	var diagName = $('#diagName').val();
	var time = $('#time').val();

	const scheduledArea = $('scheduledArea');

    //AJAX POST request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
        $.ajax({
            url: "https://" + host + "/api/CiC/diagnosticReq",
            type: "POST",
            // Setting Headers
            beforeSend: function(request) {
                request.setRequestHeader("sessionId", sessionIdCookie)
                request.setRequestHeader("gwToken", gwToken)
                request.setRequestHeader("diagName", diagName)
                request.setRequestHeader("scheduled", time)
                    },

            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (response) {
                /** @namespace response.data
                 *  @namespace response.status
                 */
                //N.B. JavaScript will **automatically** parse JSON strings into Objects!
                //This block checks to see if the AJAX request was successful, but we still need to
                //make sure our response payload doesn't have any errors
                let jsonString = JSON.stringify(response);
				document.getElementById("scheduledArea").innerHTML = response;


            },
            error: function (data, status, error) {
                console.log(data, status, error);
                document.getElementById("scheduledArea").innerHTML = ('Error Posting Scheduled Diagnostic! Please try again Later.');
                //alert("Error Posting Diagnostic Test! Please Try Again.");
            }
        });

    }

//=================================================================
//             login.html page
//=================================================================
// Function for user to Log In
// POST method
// Endpoint: api/auth/login
// HEADERS: username, password
// Returns: JSON { sid : session_id }
// GOOD --> Works

//We'll want to make sure any forms on the page aren't allowed to submit and reload the page
document.addEventListener("DOMContentLoaded", function (event) {
    $("login").submit(function (e) {
        e.preventDefault(e);//this will prevent forms from submitting and the page from reloading
    });
});
function login() {
	var host = window.location.hostname;
    var username = $('#username').val()
    var password = $('#password').val()

    //AJAX POST request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
        $.ajax({
            url: "https://" + host + "/api/auth/login",
            type: "POST",
            // Setting Headers
            beforeSend: function(request) {
                request.setRequestHeader("username", username)
                request.setRequestHeader("password", password)
                    },

            contentType: "application/json; charset=utf-8",
            dataType: "JSON",
            success: function (response) {
                /** @namespace response.data
                 *  @namespace response.status
                 */
                //N.B. JavaScript will **automatically** parse JSON strings into Objects!
                //This block checks to see if the AJAX request was successful, but we still need to
                //make sure our response payload doesn't have any errors
                let jsonString = JSON.stringify(response);

		        var cookie = "sessionId=" + response.sid; //storing sid to a cookie
                document.cookie = cookie;
				window.location.href = "https://" + host + "/manageGW.html";

            },
            error: function (data, status, error) {
                console.log(data, status, error);
                alert("Error Logging In! Please Try Again.");
            }
        });
    }

//=================================================================
//              signUp.html page
//=================================================================
// Function for user to Sign Up
// POST method
// Endpoint: /api/auth/signup
// HEADERS: username, password
// Returns: true
// GOOD --> Works

//We'll want to make sure any forms on the page aren't allowed to submit and reload the page
document.addEventListener("DOMContentLoaded", function (event) {
    $("form").submit(function (e) {
        e.preventDefault(e);//this will prevent forms from submitting and the page from reloading
    });
});

function signUp() {

    // Validate User Input
    var firstName = $("#first").val()
    var lastName = $("#last").val()
    var username = $("#user").val()
    var password = $("#pass").val()
    var confirmPass = $("#conpass").val()
    var email = $("#email").val()

    if (firstName == '' || lastName == '' || username == '' || password == '' || confirmPass == '' || email == '') {
    alert("All fields must be completed");
    }
    else if ((password.length) < 8) {
    alert("Password must be at-least 8 characters long");
    }
    else if (!(password).match(confirmPass)) {
    alert("Passwords do not match.  Please try again..");
    }
	var host = window.location.hostname;

    //AJAX POST request
    //since we're using JSON, we need to make sure to set the contentType to application/json and
    //our dataType to json
    $.ajax({
        url: "https://" + host + "/api/auth/signup",
        type: "POST",
        // Setting Headers
        beforeSend: function(request) {
           request.setRequestHeader("firstname", $('#first').val())
           request.setRequestHeader("lastname", $('#last').val())
           request.setRequestHeader("username", $('#user').val())
           request.setRequestHeader("password", $('#pass').val())
           request.setRequestHeader("confirmPass", $('#conpass').val())
           request.setRequestHeader("email", $('#email').val())
            },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            /** @namespace response.data
             *  @namespace response.status
             */
            //N.B. JavaScript will **automatically** parse JSON strings into Objects!
            //This block checks to see if the AJAX request was successful, but we still need to
            //make sure our response payload doesn't have any errors
			window.location.href = "https://" + host + "/login.html";


        },
        error: function (data, status, error) {
            console.log(data, status, error);
            alert("Error Signing Up! Please Try Again. ");
           }
        });
  }

//=================================================================
// DropDown Menu Script
$(document).ready(function(){
    $(".dropdown-toggle").dropdown();
});

//=================================================================
$(document).ready(function(){
  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#myPage']").on('click', function(event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function(){

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

  $(window).scroll(function() {
    $(".slideanim").each(function(){
      var pos = $(this).offset().top;

      var winTop = $(window).scrollTop();
        if (pos < winTop + 600) {
          $(this).addClass("slide");
        }
    });
  });
})
